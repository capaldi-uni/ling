"""Регулярные задачи"""

from . import scheduler

from mongo import *
import pipeline

from part2 import w2v

from traceback import print_exc


@scheduler.task('cron', id='update_task', minute=0)
def update_task():
    """Парсим новости раз в час"""

    with connection():

        try:
            pipeline.run()

        except Exception:
            print_exc()


@scheduler.task('cron', id='w2v_task', hour='*/3')
def w2v_task():
    """Поиск синонимов при помощи модели word2vec каждые 3 часа"""

    with connection():
        try:

            w2v()

        except Exception:
            print_exc()
