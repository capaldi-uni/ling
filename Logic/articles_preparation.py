import sys
sys.path.insert(0, '../Parsers')
sys.path.insert(1, '..')

from constants import ARTICLES_PATH
from nltk.tokenize import sent_tokenize
from mongo import Article, connection
import pymorphy2
import re
import os

def split_text_to_sentences(text):
    sent_lst=sent_tokenize(text)
    fixed_sent=[]
    for sent in sent_lst:
        fixed_str=re.sub(r"^\W+", '', sent)\
                    .replace("\n", '').replace("']", '').replace("'", '')\
                    .strip().replace(chr(93), '')
        fixed_str=re.sub(r"\W,", ",", fixed_str)
        sub_sent=fixed_str.split("... ")
        for i, snt in enumerate(sub_sent):
            not_last=i<len(sub_sent)-1
            if not_last:
                fixed_sent.append(snt+"...")
            else:
                fixed_sent.append(snt)
    return '\n'.join(fixed_sent)

def remove_excess_for_word2vec(text):
    txt=re.sub(r"[,'()*;—«»}{|!?…\"]", "", text)#.replace(' - ', ' ')
    for comb in re.findall(r"\D:", txt):
        txt=txt.replace(comb, comb[0])
    txt=re.sub(r"\.\n", "\n", txt)
    txt=re.sub(r"\.$", "", txt)
    #txt=re.sub(r"\s\d+\W\w+\s", " ", txt)
    #txt=re.sub(r"\b\d+\W\w+\b", " ", txt)
    #txt=re.sub(r"\b\d+\W\d+\b", " ", txt)
    #Если слишком чистить, то нормальных синонимов вообще не будет, а если не чистить, то будут цифры в синонимах
    """txt=re.sub(r"\s\d+\s", " ", txt)
    txt=re.sub(r"\s\d+\s", " ", txt)
    txt=re.sub(r"^\d+\s", " ", txt)
    txt=re.sub(r"\s\d+$", " ", txt)
    txt=re.sub(r"\s\w\s", " ", txt)
    txt=re.sub(r"\s\w\s", " ", txt)
    txt=re.sub(r"^\w\s", " ", txt)
    txt=re.sub(r"\s\w$", " ", txt)
    txt=re.sub(r"\s\d+\W\w+\s", " ", txt)
    txt=re.sub(r"\s\d+\W\w+\s", " ", txt)
    txt=re.sub(r"^\d+\W\w+\s", " ", txt)
    txt=re.sub(r"\s\d+\W\w+$", " ", txt)"""
    txt=re.sub(r"\s{2,}", " ", txt).replace(" .", ".")
    return txt

def normalize_text(text, morph):
    words_lst=[sent.split(' ') for sent in text.split('\n')]
    for i, sent in enumerate(words_lst):
        for j, word in enumerate(sent):
            words_lst[i][j]=morph.parse(word)[0].normal_form
    return '\n'.join([' '.join(sent) for sent in words_lst])

def get_prepared_text(article_content, morph):
    txt=split_text_to_sentences(article_content)
    txt=normalize_text(txt, morph)
    txt=remove_excess_for_word2vec(txt)
    #print(txt)
    return txt

def make_article_file(content, article_id, max_number, number):
    os.makedirs(ARTICLES_PATH, exist_ok=True)
    with open(ARTICLES_PATH+'/article_'+article_id+'.txt', 'w') as article:
        #print(content) #Можно закомментировать, чтобы не выводило на консоль
        article.write(content)
        print("========================================File "+str(number)+" of "+str(max_number)+" written========================================")

def generate_article_files(articles_id_lst, morph):
    print("========================================Generating_article_files========================================")
    #os.system("rm -R "+ARTICLES_PATH)                             #Не кросплатформенно, но. т. к. мы делаем под UBUNTU, годится
    #os.mkdir(ARTICLES_PATH)
    max_number=len(articles_id_lst)-1
    number=0
    for article_id in articles_id_lst:                                     #article.text() TypeError: 'str' object is not callable 
        lemmatized_article=get_prepared_text(str(Article.objects.get(id=article_id).content), morph)
        make_article_file(lemmatized_article, article_id, max_number, number)
        number+=1
        """if article_counter==1:
            break"""

if __name__ == '__main__':
    morph = pymorphy2.MorphAnalyzer()
    with connection():
        """for article in Article.objects:
            print(get_lemmatized_article(str(article.content), morph))
            break"""
        generate_article_files(morph)

#sudo apt install openjdk-8-jdk
#mkdir spark300
#cd spark300
#cp -R "/home/vagrant/KW/ling/Drivers/spark-3.0.0-preview2-bin-hadoop3.2.tgz" /home/vagrant/spark300/
#sudo tar -zxvf spark-3.0.0-preview2-bin-hadoop3.2.tgz
#sudo nano /etc/environment
#Добавить JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
#source /etc/environment
#sudo nano .bashrc
#Добавить
#source /etc/environment
#export PYSPARK_PYTHON=/usr/bin/python3
#export PYSPARK_DRIVER_PYTHON=python3
#export SPARK_HOME="home/vagrant/spark300/spark-3.0.0-preview2-bin-hadoop3.2"
#export PATH="$SPARK_HOME/bin:$PATH"
#source .bashrc
#sudo fallocate -l 3G /swapfile //Добавляем раздел подкачки
#ls -lh /swapfile //Проверяем, что он добавлен
#sudo chmod 600 /swapfile //Блокировка для всех, кроме суперпользователя
#sudo mkswap /swapfile //Создаем файловую систему
#sudo swapon /swapfile //Включаем файл подкачки
#sudo swapon --show //Убеждаемся, что работает
#echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab //Чтобы файл подкачки сохранялся после перезагрузки
#sudo nano /etc/environment
#_JAVA_OPTIONS="-Xms1g -Xmx4g"
#python3
#import nltk
#nltk.download('punkt')
