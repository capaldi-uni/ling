import sys
sys.path.insert(0, '..')     #Чтобы найти модуль constants, необходимо добавить путь к нему, поскольку модуль не содержится в данной директории

from mongoengine import *
import requests as req
from bs4 import BeautifulSoup
from constants import PERSONS_URL
from mongo import connection, Person
from functions import get_unique_str_ind

 
def bs4_soup_to_str(bs4_soup):
    lst=[str(item) for item in bs4_soup]
    return ' '.join(lst)

def get_text_in_tags(bs4_soup):
    lst=[]
    for item in bs4_soup:
        string=str(item)
        start=string.find('>')+1
        end=string.rfind('<')
        fio=string[start:end]                                                 #Часть может быить целиком в верхнем регистре, что исправляется в следующей строке
        fio_items_lst=[item.strip().capitalize() for item in fio.split(' ')]  #В источнике не у всех указаны Отчества
        for fio_item in fio_items_lst:
            if not fio_item[0:1].isalpha():                                   #Срезы в отличае от индесов не дают ошибок при выходе за границы (для пустой строки)
                fio_items_lst.remove(fio_item)
        lst.append(' '.join(fio_items_lst))
    return '\n'.join(lst)

def get_20_person_after_number(url, number):
    resp = req.get(url+str(number))
    soup = BeautifulSoup(resp.text, 'lxml')
    soup_tags=soup.find_all("div", {"class": "title"})
    soup_0=BeautifulSoup(bs4_soup_to_str(soup_tags), 'lxml')
    soup_a_tags=soup_0.find_all('a')
    persons=get_text_in_tags(soup_a_tags)
    return persons

def get_persons(count):
    #Количество желаемых персон должно быть кратно 20
    url=PERSONS_URL
    start_display_count=0
    max_displayed_count=count                                                  #Всего их 189 на сайте
    cur_displayed_count=start_display_count
    lst=[]
    while cur_displayed_count<max_displayed_count:
        lst.append(get_20_person_after_number(url, cur_displayed_count))
        cur_displayed_count+=20
    return '\n'.join(lst).split('\n')

def fill_persons_collection():
    print("========================================Persones_parsing========================================")
    fio_lst=get_persons(200)
    number=1
    with connection():
        Person.drop_collection()                                          
        letter_1='A'
        letter_2='A'
        for item in fio_lst:
            an_index, letter_1, letter_2 = get_unique_str_ind('PersonIndex', letter_1, letter_2)
            Person(name=item, index=an_index).save()
            print("Parsed: "+item)

if __name__ == '__main__':
    fill_persons_collection()
    #Можно посмотреть, что заполнилось раскомментировав
    """with connection():
        for person in Person.objects:
            print(person.name+' '+person.index)"""
