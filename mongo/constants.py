"""Константы модуля"""

DEFAULT_DATABASE = 'news'
DEFAULT_TIMEZONE = 'Europe/Moscow'
DATE_FORMAT = '%Y-%m-%d %H:%M'
