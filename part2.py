from posix import EX_CANTCREAT
from mongo import connection
import sys

sys.path.insert(0, './Parsers')
sys.path.insert(1, './Logic')

from word2vec import find_synonyms_for_words
import pymorphy2
from mongo import Article, Sentence, Landmark, Person, connection
from landmarks_parser import fill_landmarks_collection
from persons_parser import fill_persons_collection
from articles_preparation import generate_article_files
from dst_words_replacer import replace_dst_entities_in_articles, get_environment_ent_words,\
     remove_stop_words, generate_word2vec_model, update_environment_words_in_database, update_synonyms_in_database,\
     write_sents_with_ent_to_db

def ner(articles_id_lst):
    morph = pymorphy2.MorphAnalyzer()
    str_articles_id_lst=[str(art_id) for art_id in articles_id_lst]
    if len(Person.objects)==0:
        fill_persons_collection()
    if len(Landmark.objects)==0:
        fill_landmarks_collection()
    generate_article_files(str_articles_id_lst, morph)
    sents_with_person, sents_with_landmark = replace_dst_entities_in_articles(str_articles_id_lst, morph)
    person_env_dict, landmark_env_dict = get_environment_ent_words(sents_with_person, sents_with_landmark)
    sent_id_lst = write_sents_with_ent_to_db(sents_with_person, sents_with_landmark)
    update_environment_words_in_database(person_env_dict, landmark_env_dict)
    return sent_id_lst

def w2v(vector_size=16, min_count=1):
    remove_stop_words()
    generate_word2vec_model(vector_size, min_count)
    update_synonyms_in_database()


if __name__ == '__main__':
    with connection():
        articles_id_lst = [str(article.id) for article in Article.objects]
        #Person.drop_collection()
        #Landmark.drop_collection()
        #Sentence.drop_collection()
        sent_id_lst = ner(articles_id_lst)
        w2v()
        for sent in Sentence.objects:
            print("===============================================================")
            if sent.entity_type=="person":
                print(Person.objects(id=sent.entity_id).first().name)
            if sent.entity_type=="landmark":
                print(Landmark.objects(id=sent.entity_id).first().name)
            print(sent.content)
            print("===============================================================")
        #Персоны
        for person in Person.objects:
            print("===============================================================")
            print(person.name, person.index, person.synonyms, person.environment_words, sep='\n')
            print("===============================================================")
        #Достопримечательности
        for landmark in Landmark.objects:
            print("===============================================================")
            print(landmark.name, landmark.index, landmark.synonyms, landmark.environment_words, sep='\n')
            print("===============================================================")
