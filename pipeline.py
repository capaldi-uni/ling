"""Прототип пайплайна

Эта штука нужна, чтобы конвейером выполнять функции
Выход каждого этапа подаётся на вход следующему
Выход последнего этапа - выход пайплайна

"""
from operator import itemgetter
from typing import Callable

from parser import parse_from_page
from part2 import ner
from tonality import classify_sentences, rate

_stages = {}
_intermediate = {}


def add_stage(func: Callable, order: int):
    """Добавить этап в пайплайн"""
    _stages[order] = func


def get_intermediate(index: int):
    """Получить результат промежуточного этапа"""
    return _intermediate.get(index, 0)


def run(*args, **kwargs):
    """Запустить пайплайн с заданными аргументами"""
    if _stages:
        stages = sorted(_stages.items(), key=itemgetter(0))

        index, first = stages[0]
        rest = stages[1:]

        tmp = first(*args, **kwargs)
        _intermediate[index] = tmp

        for index, stage in rest:
            tmp = stage(tmp)
            _intermediate[index] = tmp

        return tmp

    return None


# Этот кусок добавляет парсинг в пайплайн первым этапом
add_stage(parse_from_page, 0)
add_stage(ner, 1)
add_stage(classify_sentences, 2)
add_stage(rate, 3)
